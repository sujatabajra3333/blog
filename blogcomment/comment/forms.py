from django import forms
from .models import Comment, Post

class BlogForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title','content','intro', 'image', 'slug']
       

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields=['name','content']
        widgets={
            'name':forms.TextInput(attrs={'class':'form-control','id':'nameid'}),
            'content':forms.TextInput(attrs={'class':'form-control','id':'contentid'}),
                }