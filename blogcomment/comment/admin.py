from xml.etree.ElementTree import Comment
from django.contrib import admin
from .models import  Post, Comment
# Register your models here.
@admin.register(Comment)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'name','content')

admin.site.register(Post)

