from rest_framework.routers import DefaultRouter
from .views import PostViewSet, CommentViewSet
from django.urls import path, include
router = DefaultRouter()
router.register(r'post',PostViewSet, basename='blogpost_api')
router.register(r'comment',CommentViewSet, basename='blogcomment_api')
urlpatterns = [
    path('', include(router.urls))
]
