from django.http import JsonResponse
from django.shortcuts import render,redirect
from .forms import CommentForm
from .models import Comment,Post


from rest_framework import viewsets
from .serializers import PostSerializer, CommentSerializer
#from django.views.decorators.csrf import csrf_exempt
#Create your views here.

def home(request):
    form = CommentForm()
    cmt = Comment.objects.all()
    posts = Post.objects.all()

    return render(request,'comment/home.html',{'form':form, 'comments':cmt,'posts':posts})

#@csrf_exempt
def save_data(request):
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            sid = request.POST.get('cmtid')
            name= request.POST['name']
            content= request.POST['content']
            if(sid==''): 
             usr = Comment(name= name, content=content)
            else:
             usr = Comment(id =sid, name= name, content=content)

            usr.save()
            cmt = Comment.objects.values()
            # print(emp)
            comment_data = list(cmt)
            return JsonResponse({'status': 'Save', 'comment_data': comment_data})
        else:
            return JsonResponse({'status': 0})


def delete_data(request):
    if request.method == 'POST':
        id = request.POST.get('sid')
        cmt = Comment.objects.get(pk=id)
        cmt.delete()
        return JsonResponse({'status':1})
    else:
        return JsonResponse({'status':0})

def edit_data(request):
    if request.method == 'POST':
        id = request.POST.get('sid')
        #print(id)
        comments = Comment.objects.get(pk=id)
        comments.save()
        comment_data = {"id": comments.id, "name": comments.name, "content": comments.content}
        #print(employee_data)
        #employee_data = list(employee_data)
        return JsonResponse(comment_data)


def detail(request, id):
    post = Post.objects.get(pk=id)

    if request.method == 'POST':
        form = CommentForm(request.POST)

        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            return redirect('detail', id=post.id)
    else:
        form = CommentForm()

    return render(request, 'comment/detail.html', {'post': post, 'form': form})

#API

class PostViewSet(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class CommentViewSet(viewsets.ModelViewSet):
    serializer_class = CommentSerializer
    queryset= Comment.objects.all()

