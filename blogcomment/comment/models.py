from django.db import models

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField()
    intro=models.TextField()
    content = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='uploads/', blank=True, null=True)
    class Meta:
        ordering = ['-date_added']

class Comment(models.Model):
    name = models.CharField(max_length=200)
    content = models.CharField(max_length = 30)
    
    
